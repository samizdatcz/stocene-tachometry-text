# Samopublikační šablona článků na [interaktivni.rozhlas.cz](https://interaktivni.rozhlas.cz)

> Ahoj! Já jsem šablona interaktivních rozhlasových článků! Když mě naplníš dobrým obsahem, tak ho hezky zveřejním! Jej!

Publikační systém je volně inspirován [Jekyllem](https://jekyllrb.com/docs/frontmatter/), je ale notně zjednodušený a otevřenější tweakování individuálních článků.

## Předpoklady
Chce to mít aktuální [node](https://nodejs.org/en/) a nainstalovaný [nějaký Visual Studio](https://www.visualstudio.com/en-us/products/visual-studio-community-vs.aspx). Nakonec je potřeba mít aktuální LiveScript: `npm install -g livescript`

## Nový článek

První rozdíl od Jekylla je, že každý článek je separátní repozitář. Je tedy nutné si stáhnout (nebo naklonovat) šablonu a nastavit jí gitový *remote* na nový repozitář.

- Naklonujeme repo do nového adresáře

```bash
git clone git@bitbucket.org:samizdatcz/longread-template.git jmeno-projektu
```

- Přepneme se do nového adresáře a nainstalujeme potřebné npm moduly.

```bash
$ cd jmeno-projektu
$ npm install
```

> Alternativně lze pro Windows stáhnout [zkompilovaný binárky](https://samizdat.blob.core.windows.net/storage/node_modules.zip). 

- Spustíme přejmenovávací skript, který nastaví korektní hodnoty pro Google Analytics apod.

```bash
$ npm run name
``` 

- Odstraníme současný remote (vedoucí na šablonu)

```bash
git remote remove origin
```

- Vytvoříme pro projekt nové repo. `smzd create` sám rovnou nastaví správný nový remote

```bash
smzd add jmeno-projektu
```

- To je celé! Teď už se můžeme vrhnout na psaní článku.

## Psaní článku
Celý neinteraktivní obsah se nastavuje v souboru `article.md`. Skládá se, podobně [Jekyll](https://jekyllrb.com/docs/frontmatter/), ze dvou částí: hlavičky a obsahu.

### Hlavička
Hlavička je uvozena a ukončena třemi spojovníky: `---`, uvnitř se používá [YAML](http://www.yaml.org/start.html). V zásadě je tam každá proměnná na novém řádku, její název je to co je před dvojtečkou a obsah co je za ní. Textový obsah je dobré dávat do uvozovek. Uvozovky v uvozovkách je nejlepší řešit typografickými uvozovkami.

```yaml
---
title: "Nejrelativnější „článek“"
---
```

YAML umí i pole, to se používá u seznamu autorů. To se píše jako ve většině programovacích jazyků

```yaml
authors: ["Jan Boček", "Jiří Hošek", "Jan Cibulka", "Marcel Šulek"]
```

V hlavičce jsou tyto podporované proměnné. Pokud není napsáno jinak, jsou povinné.

- `title` Nadpis článku.
- `perex` Perex.
- `description` Popis na sociální sítě (typicky trochu delší než perex).
- `authors` Seznam autorů.
- `coverimg` Odkaz na webově dosažitelný uvodní velkoobrázek. Co největší, při prvním buildu se vygenerují potřebné zmenšeniny.  Má-li se použít šablona bez obrázku, proměnná se vůbec nezadává a celý řádek se smaže.
- `socialimg` Odkaz na webově dosažitelný obrázek pro sociální sítě. Pokud se nevyplní, použije se `coverimg`. Pokud se vyplní, měl by mít rozumné rozměry, tedy ideálně nad 1200×630.
- `published` Datum, kdy byl článek publikován, případně další bláboly do patičky (dá se tam vrazit libovolné HTML).
- `url` *nice url* finální verze článku. Pro `interaktivni.rozhlas.cz/brexit` zadáme `url: brexit` (bez lomítek). Používá se na socmediální odkazy. Pokud se nevyplní, použije se plná verze (`interaktivni.rozhlas.cz/data/brexit/www`). Krátká URL se generuje přes repo [interaktivni-rozhlas-cz](https://bitbucket.org/samizdatcz/interaktivni-rozhlas-cz).
- `libraries` pole požadovaných knihoven. Podporovány jsou `[d3, jquery, highcharts, leaflet]`. Jdou psát i takhle bez uvozovek. Pokud článek knihovny nepotřebuje, proměnná se neuvádí. Je možné vložit i URL na knihovnu, nedoporučuju ale odkazovat knihovny na cizích serverech, v každém případě cíl musí být na https.
- `styles` pole požadovaných stylů, píše se celá URL, např. `styles: [https://js.arcgis.com/3.17/esri/css/esri.css]`. Opět příliš nedoporučuju odkazovat na cizí servery a opět cíl musí být na https.
- `recommended` Seznam článků v "čtěte také". U každého musí být vyplněny všechny 4 pole `link` (odkaz), `title` (nadpis), `perex` (perex) a `image` (webově dosažitelná adresa náhledového obrázku, bude automaticky zmenšena). Jednotlivé záznamy se uvozují minusem `-`.

### Obsah
Obsah se píše v [Github-flavoured Markdownu](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet). Ne všechny prvky jsou nastylovány z výroby, pokud vám nějaký bude scházet. dejte vědět.

Různé interaktivity a obrázky se vkládají přes čisté HTML, tagem `<aside>`. Ten musí mít jednu z class `big` nebo `small` – big je na celou šířku, small jde do postranního sloupce.

Pokud má interaktivita nějaký popis (zdroj…), tak se iframe/obrázek vloží do značky `<figure>` a popis do `<figcaption>`.

Příklad malého obrázku s popisem:
```html
<aside class="small">
  <figure>
    <img src="https://interaktivni.rozhlas.cz/data/brexit/charts/poslanci-small.png" width="300" height="214">
  </figure>
  <figcaption>
    Zdroj: <a href="http://www.bbc.com/news/uk-politics-eu-referendum-35616946" target="_blank">BBC</a>
  </figcaption>
</aside>
```

Příklad velkého iframu
```html
<aside class="big">
  <iframe src="https://samizdat.cz/data/brexit-euskepse-map/" class="ig" width="1024" height="710" scrolling="no" frameborder="0"></iframe>
</aside>
```

## Buildování
Celý článek buildnete příkazem
```bash
npm run build
```
Používáte-li Sublime, tak lze buildit rychleji - stačí si otevřít buildfile.ls a v něm stisknout `ctrl+b`.

Ještě rychleji to jde, pokud si nastavíte správný buildsystém na Markdownu – s otevřeným `article.md` jděte do horního menu Tools, Build System a zvolte `Markdown Samizdat deploy`. Pak stačí při psaní článku stisknout ctrl+b a jedete.

Pozor, oba případy fungují **pouze pokud si článek otevřete jako projekt**. To uděláte dvojkliknutím na `jmeno-projektu.sublime-project` a poznáte to podle toho, že vlevo budete mít lištu s adresářovou strukturou projektu.

Pro testování používejte verzi `_index.html`, **nejsou v ní analytics kódy**,  nezmrvíte tedy pageviews.

Na Macu je pak ještě potřeba upravit `jmeno-projektu.sublime-project` a přepsat tam všechny `lsc.cmd` jen na `lsc`. Klávesová zkratka pak je `cmd+b` místo `ctrl+b`.

## Deploy

```bash
git add .
git commit -m "Napsán článek"
git push
```

Přidávejte vše, co vám to nabídne, gitignore je správně nastavený. Zejména se nebojte přidat vygenerované obrázky, ulehčíte serveru, který je nebude muset generovat znova.

Článek bude za chvíli viditelný na `interaktivni.rozhlas.cz/data/jmeno-projektu/www`.