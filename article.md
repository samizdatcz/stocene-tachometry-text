---
title: "Stočený tachometr má až 70&nbsp;procent dovážených bazarových aut, varuje odborník"
perex: "Sledováním VIN a najetých kilometrů v inzerátech autobazarů napříč Evropou lze odhalit podvody. Při nákupu ojetého auta si zkontrolujte, zda není na seznamu."
description: "Sledováním VIN a najetých kilometrů v inzerátech autobazarů napříč Evropou lze odhalit podvody. Při nákupu ojetého auta si zkontrolujte, zda není na seznamu."
authors: ["Jan Boček"]
published: "23. září 2016"
coverimg: https://interaktivni.rozhlas.cz/stocene-tachometry-text/media/cover.jpg
socialimg: https://interaktivni.rozhlas.cz/stocene-tachometry-text/media/cover.jpg
coverimg_note: "Knight Rider alias KITT, hrdina legendárního seriálu z 80. let, licence CC BY-SA, <a href='https://www.flickr.com/photos/123632659@N06/13942339088/'>Federico Rebeschini | Flickr</a>"
url: "stocene-tachometry-text"
libraries: [jquery, highcharts]
recommended:
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/tri-roky-modernizace-d1-kde-to-nejvic-drnca--1559167
    title: D1: projeďte si ji kilometr po kilometru
    perex: Český rozhlas změřil intenzitu otřesů při jízdě po nejděravější české dálnici.
    image: https://samizdat.cz/data/drncani-web/www/media/cover.jpg
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/v-monaku-je-nejpomalejsi-trat-f1-presto-se-na-ni-stava-nejvice-nehod--1492883
    title: V Monaku je nejpomalejší trať F1. Přesto se na ní stává nejvíce nehod
    perex: Trať v Monaku prověřuje techniku i řidičské schopnosti na maximum. Potvrzují to i statistiky nehod a odstoupení ze závodu.
    image: http://media.rozhlas.cz/_obrazek/3192308--daniel-ricciardo-vyhral-uz-treti-letosni-zavod-formule-1--1-640x389p0.jpeg
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/na-nehodovost-ma-vliv-stari-auta-a-zkusenosti-ridice-na-znacce-nezalezi--1520240
    title: Na nehodovost má vliv stáří auta a zkušenosti řidiče, na značce nezáleží
    perex: Český rozhlas analyzoval databázi více než 800 tisíc dopravních nehod. Vyplynulo z ní, že spíš než na značce auta záleží na jeho stáří, způsobu využití a zkušenosti řidičů.
    image: http://media.rozhlas.cz/_obrazek/3430228--dopravni-nehoda-u-telce--1-666x500p0.jpeg
---

Na konci letošního července se na webu polského autobazaru Otomoto objevil inzerát na Škodu Octavia s 340 tisíci najetých kilometrů za lehce přes 50 tisíc korun. O dva týdny později stejný vůz inzeroval náchodský autobazar, tentokrát už jen se 180 tisíci kilometrů na tachometru. Téměř 160 tisíc kilometrů zmizelo. Změnila se i cena: nově auto mělo stát 86 tisíc korun.

Za poslední dva roky objevilo Sdružení na ochranu vlastníků automobilů (SOVA) podobných případů přes šest tisíc. Na svém webu nabízí jejich [kompletní seznam](http://www.sdruzeni-sova.cz/seznam-aut-s-podezrenim-na-zasah-do-stavu-tachometru), ve kterém lze vyhledávat například pomocí identifikačního čísla vozidla (VIN).

„Sledujeme inzeráty na prodej vozů z hlavních inzertních a aukčních serverů v západní Evropě,“ vysvětluje předseda SOVA Zbyněk Veselý. „Podle identifikačního čísla vozidla pak hledáme stejné vozy na českých inzertních webech a porovnáváme stav tachometru. Pokud se počet ujetých kilometrů snížil, zařadíme vůz do seznamu aut s podezřením na manipulaci s tachometrem.“
„U aut dovezených ze zahraničí má podle mých zkušeností stočený tachometr 60 až 70 procent,“ doplňuje konzultant nákupu ojetých aut Aleš Zich.

## Pozor na polské autobazary

Mezi šesti tisíci podvodů s najetými kilometry SOVA nejčastěji odhalila Octavii, následovanou Passatem a Mondeem. Hodnoty tachometru klesly o 5 až 821 tisíc kilometrů, mediánově o 36 tisíc kilometrů.

<aside class="big">
  <iframe src="https://samizdat.cz/data/stocene-tachometry/charts/modely.html" class="ig" width="1024" height="600" scrolling="no" frameborder="0"></iframe>
</aside>

Značky aut, u kterých se s ujetou vzdáleností podvádí nejčastěji, víceméně kopírují značky nejčastěji dovážených ojetin: Volkswagen, Škoda nebo Ford. Výjimkou je pouze Audi; podle dostupných dat je u této značky pravděpodobnost stočeného tachometru vyšší. Může ale jít o zkreslení, způsobené malým vzorkem dat – SOVA ročně odhalí několik tisíc podvodů, ojetin se ve stejném období doveze přes sto tisíc. Riziko podvodu u konkrétní značky tedy nelze určit zcela spolehlivě.

SOVA také eviduje, kde se inzerát na vůz se stočeným tachometrem objevil. Nejčastěji – ve 43 procentech případů – šlo o české prodejce. Mezi zahraničními dominovalo Polsko (37 procent), menší část inzerátů se objevila na Slovensku (15 procent) a v Maďarsku (4 procenta). V Německu a dalších západních zemích SOVA podvody s najetými kilometry neodhalila téměř žádné.

## STK legalizuje stočené tachometry

SOVA se soustředí pouze na ojetiny ze zahraničí. Historii vozů, které jezdily po českých silnicích, je potřeba hledat jinde: stav tachometru umožňuje kontrolovat [společnost Cebia](https://www.zkontrolujsiauto.cz/), bezplatně také [specializovaný web ministerstva dopravy](https://www.kontrolatachometru.cz/).

Kontrola stavu tachometru se totiž v posledních dvou letech stala povinnou součástí státní technické kontroly. Budoucí majitel auta si tak může ověřit, zda současný stav tachometru není nižší než při poslední STK.

Paradoxně ovšem tato novinka stáčení tachometrů často napomáhá. Při dovozu auta ze zahraničí zafixuje jeho současný stav; nekontroluje, zda nedošlo k jeho stočení v minulosti.
Změna pravomocí STK, která by zahrnovala ověření původu auta, je podle konzultanta Aleše Zicha nereálná. „Prověření auta je většinou individuální záležitost, neexistuje informační kanál, kde by se dal vůz zkontrolovat. Často je potřeba zavolat do zahraničního servisu a na to zaměstnanci STK obvykle nemají potřebné jazykové vybavení.“

V blízké budoucnosti by ovšem takový kanál mohl vzniknout.

„Stáčení tachometrů podle nás vyžaduje celoevropské řešení,“ vysvětluje Zbyněk Veselý ze SOVA. „To by mohlo přijít se zavedením jednotných STK v Evropské unii, jež by snad mělo vstoupit v platnost v polovině roku 2018. Je ale otázkou, jak odpovědně přistoupí jednotlivé země k plnění databází údaji o najetých kilometrech. Navíc tu stále bude přetrvávat praxe stáčení km před první STK – a to je manipulace, kterou nepůjde odhalit,“ dodává Veselý.

„Přesnou legislativní podobu toho, jak jednotná evropská pravidla uplatníme, teprve připravíme,“ uvádí mluvčí ministerstva dopravy Tomáš Neřold. „V každém případě výměna informací o stavu vozidla z informačních systémů STK v rámci EU je klíčová pro to, abychom měli kontrolu také nad historii dovezených vozidel v momentě jejich první registrace v ČR. Cílem je, aby tyto informace měl nejen stát, ale i lidé, kteří si auto pořizují.“

## Stáčení tachometrů má být trestný čin, říká SOVA

Podle Veselého je navíc nutné změnit stáčení tachometru na trestný čin podobně jako v sousedním Německu a na Slovensku. A to i přesto, že stáčení tachometru je ve většině případů trestné už dnes. Pokud totiž stočením tachometru zvýšíte hodnotu vozu o víc než pět tisíc korun, jde o trestný čin podvodu.

„Stávající stav umožňuje většině pachatelů se vyvinit,“ argumentuje Veselý. „Policie jim není schopna dokázat úmysl, obvykle také neexistují důkazy o tom, kdo vůz stočil. Proto se téměř žádný případ nedostane k soudu. Trestnost by měla významný preventivní rozměr, pomohla by například omezit nabídku firem, které stáčení tachometrů beztrestně inzerují.“

„Problém se stáčením tachometrů je skutečně především ve složitém dokazování těchto podvodů,“ souhlasí Neřold. „Cestu proto vidíme spíše v tom, že budeme zužovat prostor pro stáčení tachometrů co nejpřesnější informací o skutečném stavu vozidel.“

„Dalším krokem, který zvažujeme, je zavedení povinnosti servisů hlásit do jednotné databáze stav počítadla ujeté vzdálenosti při garančních, případně i některých dalších, prohlídkách,“ sděluje záměr ministerstva dopravy Neřold. „Podobný systém existuje na Slovensku. Tuto věc je ale třeba správně nastavit, aby měla efekt a nezvyšovala nadbytečně administrativu.“